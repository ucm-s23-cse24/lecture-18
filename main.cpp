#include <iostream>

using namespace std;

enum SquareState {EMPTY, PLAYER1, PLAYER2};

struct Board{
    int size;
    SquareState **grid;

    Board(){ // Default
        size = 3;

        grid = new SquareState*[size];

        for (int i = 0; i < size; i++){
            grid[i] = new SquareState[size];
        }

        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                grid[i][j] = EMPTY;
            }
        }
    }

    Board(int size){ // Overloaded constructor
        this->size = size;
        grid = new SquareState*[size];

        for (int i = 0; i < size; i++){
            grid[i] = new SquareState[size];
        }
        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                grid[i][j] = EMPTY;
            }
        }
    }

    Board(const Board& other){ // Copy constructor
        size = other.size;
        grid = new SquareState*[size];
        for (int i = 0; i < size; i++){
            grid[i] = new SquareState[size];
        }

        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                grid[i][j] = other.grid[i][j];
            }
        }
    }

    ~Board(){
        for (int i = 0; i < size; i++){
            delete[] grid[i];
        }

        delete[] grid;
    }
};

ostream& operator<<(ostream& os, const Board& board){
    for (int i = 0; i < board.size; i++){
        for (int j = 0; j < board.size; j++){
            if (board.grid[i][j] == EMPTY){
                os << '_';
            }
            else if (board.grid[i][j] == PLAYER1){
                os << 'X';
            }
            else if (board.grid[i][j] == PLAYER2){
                os << 'O';
            }
            os << " ";
        }
        os << endl;
    }
    return os;
}

int main(int argc, char* argv[]){

    // Your code here

    Board original;
    original.grid[0][0] = PLAYER1;
    original.grid[1][0] = PLAYER2;

    Board backup = original;

    Board secondBackup = backup;

    backup.grid[2][2] = PLAYER2;

    original.grid[1][1] = PLAYER1;

    cout << backup << endl;
    cout << secondBackup << endl;
    cout << original << endl;

	return 0;
}
